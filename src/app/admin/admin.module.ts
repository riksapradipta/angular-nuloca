import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialDesign } from '../material-design/material';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageusersComponent } from './manageusers/manageusers.component';
import { ManageproductsComponent } from './manageproducts/manageproducts.component';
import { UsersListComponent } from './manageusers/users-list/users-list.component';

const routes: Routes = [
  {
    path:'',
    component:AdminComponent,
    children:[
      { 
        path: 'manageusers/userlist',
        component:UsersListComponent
      },
      {
        path:'manageusers',
        component:ManageusersComponent
      },
      {
        path:'manageproducts',
        component:ManageproductsComponent
      },
      {
        path:'dashboard',
        component:DashboardComponent
      },
      {
        path:'',
        redirectTo:'/admin/dashboard',
        pathMatch:'full'
      }
    ]
  },
  
]


@NgModule({
  declarations: [AdminComponent, DashboardComponent, ManageusersComponent, ManageproductsComponent, UsersListComponent],
  entryComponents:[

  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialDesign,
  ]
})
export class AdminModule { }
