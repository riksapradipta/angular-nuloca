import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  menu=[

    {
      name:'Dashboard',
      icon:'dashboard',
      url:'/admin/dashboard'
    },

    {
      name:'Manage Users',
      icon:'manage_accounts',
      url:'/admin/manageusers'
    },

    {
      name:'Manage Products',
      icon:'shop_two',
      url:'/admin/manageproducts'
    },

    // {
    //   group:'Menu Group',
    //   children:[
    //     {
    //       name:'Image Gallery',
    //       icon:'images',
    //       url:'/admin/gallery'
    //     }
    //   ]
    // }
  ];

}
